﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UsandoValidacao.Models
{
    public class StringLengthViewModel
    {
        [StringLength(10)]
        public string StringLengthField { get; set; }
    }
}
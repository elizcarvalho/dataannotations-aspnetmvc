﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UsandoValidacao.Models
{
    public class RegularExpressionViewModel
    {
        [RegularExpression(@"[0-9]*")]
        public string OnlyNumbersField { get; set; }

        [RegularExpression(@"^[A-Z]+[a-zA-Z\s]*$")]
        public string FirstLetterUpperCaseField { get; set; }
    }
}
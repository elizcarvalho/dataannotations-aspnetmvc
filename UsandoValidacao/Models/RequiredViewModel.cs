﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UsandoValidacao.Models
{
    public class RequiredViewModel
    {
        [Required]
        public string RequiredField { get; set; }
        public string NonRequiredField { get; set; }

        [Required]
        [System.Web.Mvc.Remote("VerificarEmailNoServidor", "Validation", ErrorMessage = "Email informado já existe")]
        public string Email { get; set; }


    }
}
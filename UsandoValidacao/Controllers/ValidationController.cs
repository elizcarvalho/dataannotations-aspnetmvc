﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UsandoValidacao.Models;

namespace UsandoValidacao.Controllers
{
    public class ValidationController : Controller
    {
        // GET: Validation
        public ActionResult Index()
        {
            return View();
        }

        // GET: Validation/ValidateRequiredFields
        public ActionResult ValidateRequiredFields()
        {
            return View();
        }

        // POST: Validation/ValidateRequiredFields
        [HttpPost]
        public ActionResult ValidateRequiredFields(RequiredViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }
            return View(viewModel);
        }


        // GET: Validation/ValidateRequiredFields
        public ActionResult ValidateStringLengthFields()
        {
            return View();
        }

        // POST: Validation/ValidateRequiredFields
        [HttpPost]
        public ActionResult ValidateStringLengthFields(StringLengthViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }
            return View(viewModel);
        }


        // GET: Validation/ValidateRegularExpressionFields
        public ActionResult ValidateRegularExpressionFields()
        {
            return View();
        }

        // POST: Validation/ValidateRegularExpressionFields
        [HttpPost]
        public ActionResult ValidateRegularExpressionFields(RegularExpressionViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }
            return View(viewModel);
        }


        // GET: Validation/ValidateRegularExpressionFields
        public ActionResult DataTypeAndDisplayFormatFields()
        {
            return View();
        }

        // POST: Validation/ValidateRegularExpressionFields
        [HttpPost]
        public ActionResult DataTypeAndDisplayFormatFields(DataTypeAndDisplayFormatViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }
            return View(viewModel);
        }

        public ActionResult VerificarEmailNoServidor(string Email)
        {
            bool existe = false;
            try
            {
                existe = Email.Equals("eliz@tst.com") ? true : false;
                return Json(!existe, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
    }
}